# -*- coding: utf-8 -*-
"""
Created on Sat Jun  6 10:27:13 2020

@author: sylvain coulibaly
"""

import pandas as pd
import numpy as np
#from scipy import * 
#from scipy import array
import subprocess, os  
import sys
import cmath    # pour l'exponentielle complexe
#from math import *
import matplotlib.pyplot as plt
#from matplotlib import cm
import time
#import csv
import matplotlib.tri as tri
from mpl_toolkits.axes_grid1 import ImageGrid
try :
    from shapely.geometry import Polygon as sPolygon
except ImportError:
    print("shapely n'est pas installé" )
    



"""#                         CHOISIR LE TYPE DE BASSIN               """

Type = input("Pour choisir le type de bassin, tapez : \n 1 pour le bassin rectangulaire, \n 2 pour le bassin circulaire et \n 3 pour les 2 cercles concentriques \n : ")

## On initialise les paramètres L,l,R,R0,R1 car il les faut pour la création du fichier Parameters.txt
L=0 
l=0
R=0
R0=0
R1=0


if (int(Type) == 1):
    BasinType="Rectangular"
    L =float(input("Donnez la longueur L du bassin : \n "))
    m =int(input("Choisissez le numéro m du mode (m,n) : \n "))
    n =int(input("Choisissez le numéro n du mode (m,n) : \n "))
elif(int(Type) == 2):
    BasinType="Circular"
    R =float(input("Donnez le rayon R du bassin : \n "))
    s =int(input("Choisissez le numéro s du  mode (s,m) : \n "))
    m=int(input("Choisissez le numéro m du mode (s,m) : \n "))
elif(int(Type) == 3):
    BasinType="2circles"
    R0 =float(input("Donnez le rayon R0 du petit cercle (intérieur) :\n "))
    R1 =float(input("Donnez le rayon R1 du grand cercle (extérieur) :\n "))
    if (R0>R1 or R0==R1):
        print(" \n \t \t R1 doit être strictement supérieur à R0 !!!")
        sys.exit() 
    s=int(input("Choisissez le numéro s du  mode (s,m) : \n "))
    m =int(input("Choisissez le numéro m du mode (s,m) : \n "))
else:
    print(" \n \n  \t \t Choisissez 1, 2 ou 3 !!!")
    sys.exit()  


"#############################################################################################"
start_time = time.time()        # Debut du decompte du temps
"##############################################################################################"



    
                                  
"""                                                    #########################################################  
                                                       ##                                                     ##
                                                       ##                      Parameters                     ##
                                                       ##                                                     ##
                                                       #########################################################
"""        



l=0.5*L  # largeur du bassin rectangulaire

AdapMesh ="FALSE"  # Ecrire "TRUE" pour adapter le maillage, attention à la majuscule !
npt=100 # nombre de points (maillage freefem)

h=1.5 
g=9.81
c=np.sqrt(g*h)
alpha=1
dumpcoef = 1.  # coef de reflexion ; 1 pour la reflexion totale
amax=1

## voir le fichier meshRabi.edp pour retrouver les LABELi et LABELIi (i=1,2..4)
LABEL1=1
LABEL2=1
LABEL3=1
LABEL4=1
LABELI1=0
LABELI2=0
LABELI3=0
LABELI4=0

if( BasinType == "Rectangular"): 
    if(m==0 and n==1):
        theta = np.pi/2
        sign=-1
    elif(m==0 and n==2):
        theta =np.pi/2
        sign=-1
    elif(m==1 and n==0):
        theta = np.pi
        sign=-1
    elif(m==1 and n==1):
        theta = np.pi/4
        sign=-1
    elif(m==1 and n==2):
        theta =np.pi/4
        sign=-1
    elif(m==2 and n==0):
        theta = np.pi
        sign=-1
    elif(m==2 and n==1):
        theta = np.pi/2
        sign=-1
    else: 
        print(" \n \n  \t \t Le programme ne traite pas ce mode !!!")
        sys.exit()

if( BasinType == "Circular" or  BasinType == "2circles"): 
    if(s==0 and m==1):
        theta = np.pi
        sign=-1
        xsm=3.83170597
       # xsm=2.19714133
    elif(s==0 and m==2):
        theta = np.pi/2
        sign=1
        xsm=7.01558667
    elif(s==1 and m==0):
        theta = np.pi
        sign=-1
        xsm=1.84118378
    elif(s==1 and m==1):
        theta =np.pi# np.pi+np.pi/3
        sign=1
        xsm=5.33144277
    elif(s==1 and m==2):
        theta = 0
        sign=-1
        xsm=8.53631637
    elif(s==2 and m==0):
        theta = np.pi+np.pi/4
        sign=-1
        xsm=3.05423693
    elif(s==2 and m==1):
        theta = np.pi/2+np.pi/4
        sign=-1
        xsm=6.7061331
    else: 
        print(" \n \n  \t \t Le programme ne traite pas ce mode !!!")
        sys.exit()
    

"################################################################################################################"
script_dir = os.path.dirname(__file__)
results_dir0 = os.path.join(script_dir, 'Output_Files/')


if not os.path.isdir(results_dir0):                  ## création du dossier Output_Files s'il n'existe pas 
    os.makedirs(results_dir0)
  

"#################################################################################################################"


df1 = pd.DataFrame({'Param': [npt,BasinType,AdapMesh,L,l,R,R0,R1, alpha,sign,LABEL1,LABEL2,LABEL3,LABEL4, LABELI1,LABELI2,LABELI3,LABELI4]}, 
                              index =['npt','BasinType','AdapMesh','L','l','R','R0','R1',' alpha','sign','LABEL1','LABEL2','LABEL3','LABEL4',
                                      'LABELI1','LABELI2','LABELI3','LABELI4'])


df1.to_csv(results_dir0 +'Parameters.txt', 
          sep = '\t',
          header=None,
          index=False
          )          
       
          
          
"                        ###################################     first call of the subprocess #####################   "        


try:
    sp=subprocess.run(["FreeFem++", "meshRabi.edp",'false'], check=True, stdout=subprocess.PIPE)   # stdout=subprocess.PIPE permet de ne pas afficher le code freefem dans la console Python
    print('returncode 1:', sp.returncode)
except subprocess.CalledProcessError as err:
    print('ERROR:', err)            
    exit.sys()



"""                                         ###################      k calculation   #######################
"""


def coef_kmn(L,l,m,n) :
                    return np.sqrt((m*np.pi/L)**2 + (n*np.pi/l)**2)


def coef_ksm(xsm,r) :
                    return xsm/r





df4 = pd.read_csv(results_dir0 +'maillage.dat', sep = '\t',names = ['x', 'y'] ,header = None)     
nb_noeuds=len(df4)
h=[]      
x=[]
y=[]
x=df4['x']
y=df4['y']

k=[]
k.append(nb_noeuds)

if (BasinType=="Rectangular"):
    for ii in range(0,nb_noeuds):  
         K=coef_kmn(L,l,m,n)
         k.append(K) 


if (BasinType=="Circular"):
    for ii in range(0,nb_noeuds):  
         K=coef_ksm(xsm,R)
       #  K=(2*pi*freq)/c
         k.append(K) 

if (BasinType=="2circles"):     
    for ii in range(0,nb_noeuds):  
         K=coef_ksm(xsm,(R1-R0))
        # K=2*pi/1500
         #K=(2*pi*freq)/c
         k.append(K)

df6 = pd.DataFrame({'k': k})    
(df6['k'][0]).astype(int)
df6.to_csv(results_dir0 +'k.txt', 
          sep = '\t',
          decimal='.',
          header=None,
          index=False
          )
  
k1=[]
k2=[]
k1.append(nb_noeuds)
k2.append(nb_noeuds)

for i in range(0,nb_noeuds):
    K1= k[i+1]*np.cos(theta)           # k1=k*cos(theta)
    k1.append(K1) 
    
    K2= k[i+1]*np.sin(theta)           # k2=k*sin(theta)  
    k2.append(K2) 


           


"""
                           ##  Expression of incident wave "Uinc"
"""
Uinc=[]        
RealUinc=[]
ImUinc=[]
Uinc.append(nb_noeuds)
RealUinc.append(nb_noeuds)
ImUinc.append(nb_noeuds)
UINC=[]
UINC.append(nb_noeuds)

for i in range(0,nb_noeuds):
    #Exposant =-(k1[i+1]*x[i]+k2[i+1]*y[i])      # version rélle
    Exposant =complex(0, -(k1[i+1]*x[i]+k2[i+1]*y[i]))       # version complexe  :  Uinc=amax*exp(-1i*(k1*x+k2*y)) = amax*exp(Exposant) ;
   # Exposant =complex(0, -(k[i+1]*x[i]))
    Uinci=amax*cmath.exp(Exposant)
    
    RealUinci=Uinci.real
    
    Uinc.append(Uinci)
    RealUinc.append(RealUinci)
    
    ImUinci=Uinci.imag
    ImUinc.append(ImUinci)



for i in range(1,nb_noeuds+1):       # j'enlève la lettre  "j" de l'expression du complexe hType
    d1=(RealUinc[i],ImUinc[i])
    UINC.append(d1)
    


df7 = pd.DataFrame({'Uinc': UINC})
df7.to_csv(results_dir0 + 'Uincident.txt', 
          sep = '\t',
          header=None,
          index=False
          )


"                        ###################################     second call of the subprocess #####################   "        

try:
    sp=subprocess.run(["FreeFem++", "solverRabi.edp",'false'], check=True, stdout=subprocess.PIPE)    
    print('returncode 2:', sp.returncode)
except subprocess.CalledProcessError as err:
    print('ERROR:', err)            
    exit.sys()        
              
              
print("Temps d'exécution à la fin de l'appel du deuxième subprocess.run([FreeFem++, solverRabi.edp]) : %s secondes ---" % (time.time() - start_time))


"                                       ################################################################################## "



"""                                             #########################################################  
                                                ##                                                     ##
                                                ##             .        GRAPHIQUES                     ##
                                                ##                                                     ##
                                                #########################################################
"""

   

df8 = pd.read_csv(results_dir0 + 'sol.txt', sep = '\t',names = ['Th(i).x', 'Th(i).y','iw','rw', 'tw'] ,header = None)     

x,y,iw,rw,tw = np.loadtxt(results_dir0 + 'sol.txt', unpack=True)  # extraction des vecteurs coordonnées des noeuds et solutions du problème

"################################################################################################################"
script_dir = os.path.dirname(__file__)
results_dir = os.path.join(script_dir, 'IMAGES/')


if not os.path.isdir(results_dir):                  ## création du dossier IMAGES s'il n'existe pas 
    os.makedirs(results_dir)
"#################################################################################################################"

Lambda=round(2*np.pi/k[1])
if( BasinType == "2circles"): 
    
    ##########################
    
    
    min_radius = R0
    x=x.flatten()
    y=y.flatten()

    # Create the Triangulation; no triangles so Delaunay triangulation created.
    triang = tri.Triangulation(x, y)
    
    # Mask off unwanted triangles.
    xmid = x[triang.triangles].mean(axis=1)
    ymid = y[triang.triangles].mean(axis=1)
    mask = np.where(xmid*xmid + ymid*ymid < min_radius*min_radius, 1, 0)
    triang.set_mask(mask)

    fig = plt.figure(figsize=(14, 7.5))
    
    grid = ImageGrid(fig, 111,          # as in plt.subplot(111)
                     nrows_ncols=(1,2),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="7%",
                     cbar_pad=0.15,
                     
                     )
    
    valmax = max( max(rw), max(tw))
    valmin = min( min(rw), min(tw))
    imax = [max(rw), max(tw)].index(valmax)
    
    im1 = grid[0].tricontourf(triang, rw, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    im2 = grid[1].tricontourf(triang, tw, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    im = [im1, im2]
    grid[0].set_title("Champ de vagues réfléchies \n  ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R_{0}$="+str(round(R0))+ "$m$""   $R_{1}$="+str(round(R1))+ "$m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$")
    grid[1].set_title("Champ de vagues totales \n ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R_{0}$="+str(round(R0))+ "$m$""   $R_{1}$="+str(round(R1))+ "$m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$")
    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
        #ax.plot(x,y.fl, color = 'k', lw = 1.5)			# Adds boundary
    
    
    # Colorbar
    grid[imax].cax.colorbar(im[imax])
    grid[imax].cax.toggle_label(True)
    
    #fig_name="3_wave_fields_LR_"+str(L)+".png"" $m$"
    fig_name="2_wave_fields_LR_"+str(round(np.degrees(theta)))+".png"
    plt.savefig(results_dir + fig_name)
 #   plt.show()
    
    
    fig = plt.figure(figsize=(7, 7.5))
    grid = ImageGrid(fig,111,           # as in plt.subplot(111)
                     nrows_ncols=(1,1),
                     axes_pad=0.25,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="6%",
                     cbar_pad=0.15,
                     )
    champs=rw
    fig_name="Champ de vagues réfléchies"
    valmax = max(champs)
    valmin = min(champs)
    imax = [max(champs)].index(valmax)
                    
    im0 = grid[0].tricontourf(triang, champs, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    
    im=[im0]
    grid[0].set_title(fig_name + "\n ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R_{0}$="+str(round(R0))+ "$m$""   $R_{1}$="+str(round(R1))+ "$m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$")
    
    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
        #ax.plot(x,y, color = 'k', lw = 1.5)			# Adds boundary
    
    # Colorbar
    
    grid[0].cax.colorbar(im[imax])
    grid[0].cax.toggle_label(True)
    
    plt.savefig(results_dir + fig_name)
   # plt.show()

    fig = plt.figure(figsize=(5.8,5.8))

    grid = ImageGrid(fig,111,           # as in plt.subplot(111)
                     nrows_ncols=(1,1),
                     axes_pad=0.25,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="6%",
                     cbar_pad=0.15,
                     )
    champs=tw # total wave

    fig_name="Champ de vagues totales"
    #fig_name= " "
    valmax = max(champs)
    valmin = min(champs)
    imax = [max(champs)].index(valmax)
                    
    im0 = grid[0].tricontourf(triang, champs, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    
    im=[im0]
    
    grid[0].set_title(fig_name + "\n ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R_{0}$="+str(round(R0))+ " $m$""   $R_{1}$="+str(round(R1))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$")
    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
        #ax.plot(x,y, color = 'k', lw = 1.5)			# Adds boundary
    
    # Colorbar
    
    grid[0].cax.colorbar(im[imax])
    grid[0].cax.toggle_label(True)
    filename="Tot_wave_field_2C_"+str(s)+str(m)+".png"
    plt.savefig(results_dir + filename)
    plt.show()
##########################################
if ( BasinType != "2circles"):
    
    x, y = np.loadtxt(results_dir0 + "xy_echantillonnees.txt", delimiter = "\t" , unpack=True) 
    outline = sPolygon(zip(x,y)).buffer(.1)

    
    x,y,iw,rw,tw = np.loadtxt(results_dir0 + 'sol.txt', unpack=True)
    triang2 = tri.Triangulation(x,y)
    
    ##masking
    mask = [
        not outline.contains(sPolygon(zip(x[tri], y[tri]))) 
        for tri in triang2.get_masked_triangles()
    ]
    triang2.set_mask(mask)
    
    # Set up figure and image grid
    fig = plt.figure(figsize=(18, 8))
    
    grid = ImageGrid(fig, 111,          # as in plt.subplot(111)
                     nrows_ncols=(1,2),
                     axes_pad=0.15,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",#"single",
                     cbar_size="7%",
                     cbar_pad=0.15,
                     )
    
    valmax = max( max(rw), max(tw))
    valmin = min( min(rw), min(tw))
    imax = [max(rw), max(tw)].index(valmax)
    
    
    im1 = grid[0].tricontourf(triang2, rw, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    im2 = grid[1].tricontourf(triang2, tw, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)

    im = [im1, im2]
    if(BasinType=="Rectangular"):
        grid[0].set_title("Champ de vagues réfléchies  \n  ($m,n$) =" "(" +str(m) + "," +str(n)+")" "   $L$="+str(round(L))+ " $m$""   $l$="+str(round(l))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$")
        grid[1].set_title("Champ de vagues totales   \n  ($m,n$) =" "(" +str(m) + "," +str(n)+")" "   $L$="+str(round(L))+ " $m$""   $l$="+str(round(l))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$")
    if(BasinType=="Circular"):
        grid[0].set_title("Champ de vagues réfléchies   \n  ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R$="+str(round(R))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$=" +str(Lambda)+ " $m$")
        grid[1].set_title("Champ de vagues totales    \n  ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R$="+str(round(R))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$=" +str(Lambda)+ " $m$")
  #  grid[2].set_title("Total Wave field with  m="+str(m)+ " and n="+str(n))
    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
       # ax.plot(xb,yb, color = 'k', lw = 1.5)			# Adds boundary
    
    
    # Colorbar
    grid[imax].cax.colorbar(im[imax])
    grid[imax].cax.toggle_label(True)
    if(BasinType=="Rectangular"):
        filename="2_wave_fields_RB_"+str(m)+str(n)+".png"
    if(BasinType=="Circular"):
        filename="2_wave_fields_CB_"+str(s)+str(m)+".png"
    
    plt.savefig(results_dir + filename)
   # plt.show()
    
    
    
    ################################
    
    x, y = np.loadtxt(results_dir0 + "xy_echantillonnees.txt", delimiter = "\t" , unpack=True) 

    outline = sPolygon(zip(x,y)).buffer(.1)
    
    x,y,iw,rw,tw = np.loadtxt(results_dir0 + 'sol.txt', unpack=True)
    triang2 = tri.Triangulation(x,y)
    
    ##masking
    mask = [
        not outline.contains(sPolygon(zip(x[tri], y[tri])))
        for tri in triang2.get_masked_triangles()
    ]
    triang2.set_mask(mask)
    
    # Set up figure and image grid
    fig = plt.figure(figsize=(8, 8))
    
    grid = ImageGrid(fig,111,           # as in plt.subplot(111)
                     nrows_ncols=(1,1),
                     axes_pad=0.25,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="6%",
                     cbar_pad=0.15,
                     )
    
    champs=iw
    if(BasinType=="Rectangular"):
        fig_name="Champ de vagues incidentes   \n  ($m,n$) =" "(" +str(m) + "," +str(n)+")" "   $L$="+str(round(L))+ " $m$""   $l$="+str(round(l))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$"
    if(BasinType=="Circular"):
        fig_name="Champ de vagues incidentes    \n  ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R$="+str(round(R))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda)+ " $m$"

    valmax = max(champs)
    valmin = min(champs)
    imax = [max(champs)].index(valmax)
                    
    im0 = grid[0].tricontourf(triang2, champs, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    
    im=[im0]
    grid[0].set_title(fig_name)
    
    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
        #ax.plot(xb,yb, color = 'k', lw = 1.5)			# Adds boundary
    
    # Colorbar
    
    grid[0].cax.colorbar(im[imax])
    grid[0].cax.toggle_label(True)
    if(BasinType=="Rectangular"):
        filename="inc_wave_field_RB_"+str(m)+str(n)+".png"
    if(BasinType=="Circular"):
        filename="inc_wave_field_CB_"+str(s)+str(m)+".png"
    plt.savefig(results_dir + filename)
  #  plt.show()
    
        
    
    
    triang2 = tri.Triangulation(x,y)
    
    ##masking
    mask = [
        not outline.contains(sPolygon(zip(x[tri], y[tri])))
        for tri in triang2.get_masked_triangles()
    ]
    triang2.set_mask(mask)
    
    # Set up figure and image grid
    fig = plt.figure(figsize=(8, 8))
    
    grid = ImageGrid(fig,111,           # as in plt.subplot(111)
                     nrows_ncols=(1,1),       
                     share_all=True,
                     add_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="6%",
                     cbar_pad=0.15,
                     )
        
    champs=rw
    if(BasinType=="Rectangular"):
        fig_name="Champ de vagues réfléchies   \n  ($m,n$) =" "(" +str(m) + "," +str(n)+")" "   $L$="+str(round(L))+ " $m$""   $l$="+str(round(l))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$"
    if(BasinType=="Circular"):
        fig_name="Champ de vagues réfléchies   \n  ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R$="+str(round(R))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda)+ " $m$"

    valmax = max(champs)
    valmin = min(champs)
    imax = [max(champs)].index(valmax)
                    
    im0 = grid[0].tricontourf(triang2, champs, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    
    im=[im0]
    grid[0].set_title(fig_name)
    
    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
       # ax.plot(xb,yb, color = 'k', lw = 1.5)			# Adds boundary
    
    # Colorbar
    grid[0].cax.colorbar(im[imax])
    grid[0].cax.toggle_label(True)
    if(BasinType=="Rectangular"):
        filename="Refl_wave_field_RB_"+str(m)+str(n)+".png"
    if(BasinType=="Circular"):
        filename="Refl_wave_field_CB_"+str(s)+str(m)+".png"
    plt.savefig(results_dir + filename)
   # plt.show()
     
    
    
    triang2 = tri.Triangulation(x,y)
    
    ##masking
    mask = [
        not outline.contains(sPolygon(zip(x[tri], y[tri])))
        for tri in triang2.get_masked_triangles()
    ]
    triang2.set_mask(mask)
    
    # Set up figure and image grid
    fig = plt.figure(figsize=(5.8, 5.8))
    
    grid = ImageGrid(fig,111,           # as in plt.subplot(111)
                     nrows_ncols=(1,1),
                     axes_pad=0.25,
                     share_all=True,
                     cbar_location="right",
                     cbar_mode="single",
                     cbar_size="6%",
                     cbar_pad=0.15,
                     )
    
    champs=tw
    if(BasinType=="Rectangular"):
       fig_name="Champ de vagues totales   \n  ($m,n$) =" "(" +str(m) + "," +str(n)+")" "   $L$="+str(round(L))+ "$m$""   $l$="+str(round(l))+ "$m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda) +" $m$"
    if(BasinType=="Circular"):
       fig_name="Champ de vagues totales    \n  ($s,m$) =" "(" +str(s) + "," +str(m)+")" "   $R$="+str(round(R))+ " $m$"  "\n $\u03B8$=" +str(round(np.degrees(theta)))+ "°" "   $\lambda$="+str(Lambda)+ " $m$"
    valmax = max(champs)
    valmin = min(champs)
    imax = [max(champs)].index(valmax)
                    
    im0 = grid[0].tricontourf(triang2, champs, cmap=plt.cm.jet, vmax=valmax, vmin=valmin)
    
    im=[im0]
    grid[0].set_title(fig_name)
    

    
    for ax in grid:
        ax.axis('off')						# Removes axis and frame
       # ax.plot(xb,yb, color = 'k', lw = 1.5)			# Adds boundary
       
    
    # Colorbar
   # divider = make_axes_locatable(ax1)
    #cax1 = divider.append_axes("right", size="5%", pad=0.05)
 
    grid[0].cax.colorbar(im[imax])
    grid[0].cax.toggle_label(True)
    if(BasinType=="Rectangular"):
        filename="Tot_wave_field_RB_"+str(m)+str(n)+".png"
    if(BasinType=="Circular"):
        filename="Tot_wave_field_CB_"+str(s)+str(m)+".png"
    plt.savefig(results_dir + filename)
    plt.show()

    
print("Temps final d'exécution : %s secondes ---" % (time.time() - start_time))


